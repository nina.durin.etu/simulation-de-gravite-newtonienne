# Simulateur de gravité Newtonienne

![Capture d'écran du programme](./Capture_d_écran_1.png)  
![Capture d'écran du programme](./Capture_d_écran_2.png)  

## Introduction

Ce logiciel simule un système de planètes tournant autour d'un astre central. Il permet la visualisation des equations gravitationnelles de Newton que nous avons tous appris à l'école. Un zoom central ainsi qu'une carte des vecteurs vitesses montrent plus simplement certains phénomènes.

## Fonctionnalités

 - Simulation de la gravitation telle que présentée par Newton
 - Une position et vitesse aléatoires pour chaque début de session
 - Zoom central pour visualiser le lieu où les vitesses sont les plus élevées
 - Graphe circulaire montrant les directions, sens et intensité des vecteurs vitesse des astres
 - Démonstration du mouvement de [**précéssion de l'astre**](https://fr.wikipedia.org/wiki/Pr%C3%A9cession) pris en compte dans les calculs
 - Intéraction avec un curseur pour sélectionner l'astre à surligner pour le surveiller

## Cas d'utilisation

Il peut servir à toute personne apprennat la programmation et souhaitant s'exercer à l'application des maths à l'algorithmique. Il sert aussi à montrer comment fonctionne la programmation à des enfants, ou même illustrer le phénomène de [**précession de l'astre**](https://fr.wikipedia.org/wiki/Pr%C3%A9cession) à des plus grands. Enfin, avec ses couleurs et ses caractéristiques aléatoires, il fait un très bon écran de sauvegarde.

## Prérequis

 - [Python](https://www.python.org/)
 - Module Pygame pour python (`pip install pygame`)

## Installation

`pip install pygame`  
`git clone https://gitlab.univ-lille.fr/nina.durin.etu/simulation-de-gravite-newtonienne.git`  

## Démarrage rapide

`cd simulation-de-gravite-newtonienne`  
`python3 main.py`  

## Licence

`CC-0`  
Ce projet est sous licence libre CC-0, il peut donc être librement partagé, modifié, utilisé commercialement sans aucune attribution.
