import pygame
import time
from pygame.locals import *
from random import randrange, random
from math import *

pygame.init()

screen_info = pygame.display.Info()
width, height = screen_info.current_w, screen_info.current_h

mask_radius = 100
mask_posx, mask_posy = mask_radius, mask_radius
mask2_posx, mask2_posy = mask_radius*3, mask_radius
mask_color = (10,10,10)

window = pygame.display.set_mode((width, height), FULLSCREEN)

pygame.key.set_repeat(400, 30)
pygame.mouse.set_visible(True)
clock = pygame.time.Clock()
mask = pygame.Surface((mask_radius, mask_radius))
mask3 = pygame.Rect((width-60, height-30), (60, 30))
font = pygame.font.SysFont(None, 30, bold=False)

mask.fill((0,0,0))
#mask = pygame.Rect(mask_pos, (mask_width, mask_height))
window.fill((0,0,0))

continuer = True
imax = 400
choiced = 0
factor = 0
G = 0.15
pos_x = []
pos_y = []
prepos_x = []
prepos_y = []
color = []
alpha = []
v_x = []
v_y = []
direc_x = []
direc_y = []



for i in range(0,imax):
    alpha.append(random()*6)
    factor = random() / 12
    v_x.append(cos(alpha[i])*factor)
    v_y.append(sin(alpha[i])*factor)
    direc_x.append(0)
    direc_y.append(0)
    miniangle = random()*6
    minirand = random()*width / 2
    pos_x.append((width/2) +cos(miniangle)*minirand)
    pos_y.append((height/2)+sin(miniangle)*minirand)
    prepos_x.append(pos_x[i])
    prepos_y.append(pos_y[i])
    color.append((randrange(0,255,1),randrange(0,255,1),randrange(0,255,1)))
    pass
pass


def calculateGrav():
    for i in range(0, imax):
        distance = sqrt(((width/2)-pos_x[i])**2+((height/2)-pos_y[i])**2)
        #distance = sqrt((souris_x-pos_x[i])**2+(souris_y-pos_y[i])**2)
        if distance > 2:
            force = G / distance**2
            alpha[i] = atan2(((height/2)-pos_y[i]),((width/2)-pos_x[i]))
            #alpha[i] = atan2((souris_y-pos_y[i]),(souris_x-pos_x[i]))
            direc_x[i] = cos(alpha[i])*force
            direc_y[i] = sin(alpha[i])*force
        else:
            direc_x[i], direc_y[i] = 0, 0
            v_x[i], v_y[i] = 0, 0

        v_x[i] += direc_x[i]
        v_y[i] += direc_y[i]
        prepos_x[i] = pos_x[i]
        prepos_y[i] = pos_y[i]
        pos_x[i] += v_x[i]
        pos_y[i] += v_y[i]
        pass
    pass

def calculateMultiGrav():
    for i in range(0,imax):
        for j in range(0, imax):
            if j != i:
                distance = sqrt((pos_x[j]-pos_x[i])**2+(pos_y[j]-pos_y[i])**2)
                if distance > 0.3: # tout le temps vrai sauf si les deux points se confondent
                    force = G / distance**2
                    alpha[i] = atan2((pos_y[j]-pos_y[i]),(pos_x[j]-pos_x[i]))
                    direc_x[i] = cos(alpha[i])*force
                    direc_y[i] = sin(alpha[i])*force
                    pass
                v_x[i] += direc_x[i]
                v_y[i] += direc_y[i]
                prepos_x[i] = pos_x[i]
                prepos_y[i] = pos_y[i]
                pos_x[i] += v_x[i]
                pos_y[i] += v_y[i]
                pass
        pass




while continuer == True:


    calculateGrav()

    for i in range(0,imax):
        distance = sqrt(((width/2)-pos_x[i])**2+((height/2)-pos_y[i])**2)
        if distance < 30:
            x = mask2_posx-(((width/2)-pos_x[i])*3.2)
            y = mask2_posy-(((height/2)-pos_y[i])*3.2)
            pygame.draw.circle(window, mask_color, (mask2_posx,mask2_posy), mask_radius, 5)
            pygame.draw.circle(window, color[i], (int(x),int(y)), 1)
        pass

    #window.fill((0,0,0))
    pygame.draw.circle(window, mask_color, (mask_posx,mask_posy), mask_radius)
    #pygame.draw.circle(window, mask_color, (mask2_posx,mask2_posy), mask_radius)
    pygame.draw.rect(window, (0,0,0), mask3)
    text = font.render(str(choiced), True, color[choiced])
    window.blit(text,(width-30,height-20))
    for i in range(0, imax):
        if i == choiced:
            pygame.draw.line(window, color[i], (mask_radius,mask_radius), (mask_radius + v_x[i]*(mask_radius/1),mask_radius + v_y[i]*(mask_radius/1)),3)
            pygame.draw.line(window, color[i], (prepos_x[i],prepos_y[i]), (pos_x[i],pos_y[i]),3)
        else:
            if abs(v_x[i]) + abs(v_y[i]) >= 0.1:
                pygame.draw.line(window, color[i], (mask_radius,mask_radius), (mask_radius + v_x[i]*(mask_radius/1),mask_radius + v_y[i]*(mask_radius/1)))
            pygame.draw.line(window, color[i], (prepos_x[i],prepos_y[i]), (pos_x[i],pos_y[i]),1)
        pass
    pygame.display.flip()
    clock.tick(1000)
    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                continuer = False
            if event.key == K_RIGHT:
                if choiced < imax-1: choiced += 1
            if event.key == K_LEFT:
                if choiced >= 0: choiced -= 1
            if event.key == K_SPACE:
                window.fill((0,0,0))
        if event.type == MOUSEBUTTONDOWN and event.button == 1:
            window.fill((0,0,0))
        if event.type == QUIT:
            continuer = False
        souris_x, souris_y = pygame.mouse.get_pos()
        pass

    continue
